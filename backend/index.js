//Config
// if (process.env.NODE_ENV !== "DEVELOPMENT") {
//   require("dotenv").config({ path: ".env" });
// }

require("dotenv").config({ path: ".env" })

const API = process.env.NODE_ENV_DEV === 'DEVELOPMENT' ? 
'http://localhost:3000' : process.env.PORT

const cloudinary = require("cloudinary");
const expressFileUpload = require("express-fileupload");
const express = require("express");
const app = express();
const path = require("path");
const cors = require('cors');
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const connectDB = require("./Config/connection");
const userRoutes = require("./routes/userRoute");
const productRoute = require("./routes/productRoute");
const categoryRoute = require("./routes/categoryRoute");

//Body Parser
app.use(bodyParser.urlencoded({ limit: "200mb", extended: true }));

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', 'https://despro2-backend.vercel.app'); 
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

app.use(cors({
	origin: "https://despro2.vercel.app",
	credentials: true
}))

//Cookies Parser
app.use(cookieParser());

//Database Connect
connectDB();
//JSON
app.use(express.json());

//Use Express File Upload
app.use(expressFileUpload());

//Config Cloudniary
cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key: process.env.CLOUD_API_KEY,
  api_secret: process.env.CLOUD_API_SECRET_KEY,
});

app.listen(process.env.PORT, () => {
  console.log(`Server Running At ${API}`);
});

//Load Route
app.use("/api/user", userRoutes);
app.use("/api/product", productRoute);
app.use("/api/category", categoryRoute);

// //Access Front End Static Files
app.use(express.static(path.join(__dirname, "../dist")));

//Access Front End All URL
app.get("/*", (req, res) => {
  //Access Front End Static Files
  res.sendFile(path.join(__dirname, '..', 'dist', 'index.html'));
});

//author: maryll