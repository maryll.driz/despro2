import React from "react";
import Footer from "../../Components/Footer/Footer";
import Header from "../../Components/Header/Header";
import "./About.css";
// import MetaData from "../MetaData";

const About = () => {
  const visitFacebook = () => {
    window.location = "https://facebook.com/";
  };
  document.title = "About Us";
  return (
    <>
      <Header />

      <div className="about-section-container">
        <h1 className="Heading">
          About <span>Us</span>
        </h1>
        {/* <MetaData title={'About Us'} /> */}
        <div className="about-section-box">
          <div>
            <div>
              <img
                style={{ width: "15rem", height: "15rem", margin: "2rem 0" }}
                src="https://res.cloudinary.com/dmbpungbx/image/upload/v1706253523/fquh3wgo460lb6v4h2yt.png"
                alt="Founder"
              />
              <h1>Mark, Maryll & Pat</h1>
              <button onClick={visitFacebook}>Visit Facebook</button>
              <br />
              <p>
                This is a sample wesbite made by MMP Group. 
                In Fulfillment of our Design Project 2
              </p>
            </div>
          </div>
        </div>
      </div>
      <Footer />
    </>
  );
};

export default About;
