import React from "react";
import "./Footer.css";
import { RiShoppingBasketFill, RiMapPin2Line } from "react-icons/ri";
import { SlSocialInstagram } from "react-icons/sl";
import { TiSocialTwitter, TiSocialLinkedin } from "react-icons/ti";
import { FaFacebookSquare } from "react-icons/fa";
import { MdPhoneForwarded, MdOutlineMarkEmailRead } from "react-icons/md";
import { AiOutlineArrowRight } from "react-icons/ai";
import { Link } from "react-router-dom";
import CreditCard from "../../../src/Assets/Images/payment.png";
import { useState } from "react";

const Footer = () => {
  const [message, setMessage] = useState();
  const sendMessage = () => {
    window.location = `http://m.me/maryllkate?text=${message}`;
  };
  return (
    <>
      <footer>
        <section className="footer">
          <div className="box-container">
            <div className="box">
              <h3>
                <i>{<RiShoppingBasketFill />}</i> Puremart - South City Homes Binan
              </h3>
              <p>
                Good And Convenient, We Make Your Purchase Easy By Providing
                Good Quality Products, Snacks And Beverages.
              </p>
              <div className="share">
                <Link>
                  <i
                    onClick={() => {
                      window.location = `https://www.facebook.com/`;
                    }}
                  >
                    {<FaFacebookSquare />}
                  </i>
                </Link>
                <a href="https://instagram.com/">
                  <i>{<SlSocialInstagram />}</i>
                </a>
                <Link to="#">
                  <i>{<TiSocialTwitter />}</i>
                </Link>
                <Link>
                <a href="https://www.linkedin.com/in/maryllkate/">
                  <i>{<TiSocialLinkedin />}</i>
                </a>
                </Link>
              </div>
            </div>

            {/* Contact Section */}

            <div className="box">
              <h3>Contact Us </h3>
              <Link className="links">
                <i>
                  <MdPhoneForwarded />
                </i>
                +63 9670106364
              </Link>

              <a href="mailto:maryll.driz@gmail.com" className="links">
                <i>
                  <MdOutlineMarkEmailRead />
                </i>
                maryll.driz@gmail.com
              </a>

              <Link className="links">
                <i>
                  <RiMapPin2Line />
                </i>
                Binan Laguna, PH -4024
              </Link>
            </div>

            {/* Quick Link */}

            <div className="box">
              <h3>Quick Link </h3>
              <Link to="/" className="links">
                <i>
                  <AiOutlineArrowRight />
                </i>
                Home
              </Link>

              <Link to={"/"} className="links">
                <i>
                  <AiOutlineArrowRight />
                </i>
                Features
              </Link>

              <Link to="/products" className="links">
                <i>
                  <AiOutlineArrowRight />
                </i>
                Products
              </Link>

              <Link to="/reviews/all" className="links">
                <i>
                  <AiOutlineArrowRight />
                </i>
                Reviews
              </Link>
            </div>
            {/* Quick Chat */}
            <div className="box">
              <h3>Let's Chats </h3>
              <p>Message me on messenger web</p>
              <input
                type="text"
                className="whats-message"
                placeholder="Enter Your Message..."
                onChange={(e) => setMessage(e.target.value)}
              />

              <button className="sendMsgBtn" onClick={sendMessage}>
                Send
              </button>
              <img src={CreditCard} alt="Payment Img" className="payment-img" />
            </div>
          </div>
          <div className="credit">
            Despro2 Of <span>MMP Group</span> | All Right Reserved 2024
          </div>
        </section>
      </footer>
    </>
  );
};

export default Footer;
